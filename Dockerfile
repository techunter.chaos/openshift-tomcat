FROM openshift/base-centos7
MAINTAINER https://gitlab.com/techunter.chaos/openshift-tomcat
EXPOSE 8080
ENV TOMCAT_VERSION=7.0.75 \
    MAVEN_VERSION=3.3.9
LABEL io.k8s.description="Platform for running Java applications on Tomcat 7" \
      io.k8s.display-name="Tomcat 7.0.75 JRE7" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,tomcat,tomcat7" \
      io.openshift.s2i.destination="/opt/s2i/destination"
# Install Maven, tomcat
RUN INSTALL_PKGS="tar unzip bc which lsof java-1.7.0-openjdk java-1.7.0-openjdk-devel" && \
    yum install -y --enablerepo=centosplus $INSTALL_PKGS && \
    rpm -V $INSTALL_PKGS && \
    yum clean all -y && \
    (curl -v https://www.apache.org/dist/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz | \
    tar -zx -C /usr/local) && \
    ln -sf /usr/local/apache-maven-$MAVEN_VERSION/bin/mvn /usr/local/bin/mvn && \
    mkdir -p $HOME/.m2 && \
    mkdir -p /tomcat && \
    (curl -v http://mirror.easyname.ch/apache/tomcat/tomcat-7/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz | tar -zx --strip-components=1 -C /tomcat) && \
    mkdir -p /opt/s2i/destination

# Add s2i customizations

#### Maven setting customization
 ADD ./contrib/settings.xml $HOME/.m2/

# Copy the S2I scripts from the specific language image to $STI_SCRIPTS_PATH
COPY ./s2i/ $STI_SCRIPTS_PATH
RUN chown -R 1001:0 /tomcat && chown -R 1001:0 $HOME && \
    chmod -R ug+rw /tomcat && \
    chmod -R g+rw /opt/s2i/destination
RUN chmod +x $STI_SCRIPTS_PATH/assemble $STI_SCRIPTS_PATH/run $STI_SCRIPTS_PATH/save-artifacts $STI_SCRIPTS_PATH/usage
USER 1001
CMD $STI_SCRIPTS_PATH/usage